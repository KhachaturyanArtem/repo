package com.ub.nextcargo.domen;

import lombok.Getter;
import java.time.LocalDateTime;

/*
* Этап маршрута
*
* voyage: Рейс (Ссылка)
* loadLocation: Место загрузки
* unloadLocation: Место разгрузки
* loadTime: Время загрузки
* unloadTime: Время разгрузки
*
*/

@Getter
public final class RouteStage {
    private Voyage voyage;
    private Location loadLocation, unloadLocation;
    private LocalDateTime loadTime, unloadTime;

    public RouteStage(Voyage voyage, Location loadLocation, Location unloadLocation, LocalDateTime loadTime, LocalDateTime unloadTime) {
        this.voyage = voyage;
        this.loadLocation = loadLocation;
        this.unloadLocation = unloadLocation;
        this.loadTime = loadTime;
        this.unloadTime = unloadTime;
    }
}
