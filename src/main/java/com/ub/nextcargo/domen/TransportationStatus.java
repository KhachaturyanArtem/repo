package com.ub.nextcargo.domen;

// Статус транспортировки

public enum TransportationStatus {
    NOT_RECEIVED,
    IN_PORT,
    ON_BOARD,
    CLAIMED,
    UNKNOWN
}
