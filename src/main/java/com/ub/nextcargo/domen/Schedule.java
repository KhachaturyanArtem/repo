package com.ub.nextcargo.domen;

import lombok.Getter;
import java.util.List;

/*
* Расписание рейса
*
* carrierMovements: передвижения грузового средства
*/


@Getter
public final class Schedule{
    private List<CarrierMovement> carrierMovements;

    public Schedule(List<CarrierMovement> carrierMovements) {
        this.carrierMovements = carrierMovements;
    }
}
