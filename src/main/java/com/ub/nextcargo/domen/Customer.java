package com.ub.nextcargo.domen;

import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;
import java.time.LocalDate;


/*
* Клиент
* id : ObjectId
* lastName : Фамилия
* firstName : Имя
* middleName : Отчество
* birthDay : Дата рождения
*/


@Getter
@Setter
public class Customer {
    private ObjectId id;
    private String firstName, lastName, middleName;
    private LocalDate birthDay;

    public Customer(String firstName, String lastName, String middleName, LocalDate birthDay) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.birthDay = birthDay;
    }
}
