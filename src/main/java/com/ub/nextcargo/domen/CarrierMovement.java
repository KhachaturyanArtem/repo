package com.ub.nextcargo.domen;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/*
* Передвижения
*
* departureLocation : Место отправления
* arrivalLocation : Место прибытия
* departureTime : Время отправления
* arrivalTime : Время прибытия
*/


@Getter
@Setter
public final class CarrierMovement {
    private Location departureLocation, arrivalLocation;
    private LocalDateTime departureTime, arrivalTime;

    public CarrierMovement(Location departureLocation, Location arrivalLocation,
                           LocalDateTime departureTime, LocalDateTime arrivalTime) {
        this.departureLocation = departureLocation;
        this.arrivalLocation = arrivalLocation;
        this.departureLocation = departureLocation;
        this.arrivalTime = arrivalTime;
    }
}
