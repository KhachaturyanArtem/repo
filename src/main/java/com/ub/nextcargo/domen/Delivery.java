package com.ub.nextcargo.domen;

import lombok.Getter;
import javax.xml.stream.Location;
import java.time.LocalDateTime;

/* Delivery - Доставка
* currentVoyage: Текущий рейс
* lastKnownLocation: Последнее место нахождения груза
* transportationStatus: Статус транспортировки (NOT_RECEIVED, IN_PORT, ON_BOARD, CLAIMED,UNKNOWN)
* routingStatus: Статус маршрутизации (NOT_ROUTED, ROUTED, MISROUTED)
* calculatedAt: Дата и время расчета доставки
*/

@Getter
public final class Delivery {
    private Voyage currentVoyage;
    private Location lastKnownLocation;
    private TransportationStatus transportationStatus;
    private RoutingStatus routingStatus;
    private LocalDateTime calculatedAt;

    public Delivery(Voyage currentVoyage, Location lastKnownLocation,
                    TransportationStatus transportationStatus, RoutingStatus routingStatus,
                    LocalDateTime calculatedAt) {
        this.currentVoyage = currentVoyage;
        this.lastKnownLocation = lastKnownLocation;
        this.transportationStatus = transportationStatus;
        this.routingStatus = routingStatus;
        this.calculatedAt = calculatedAt;
    }
}
