package com.ub.nextcargo.domen;

import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;
import javax.xml.stream.Location;
import java.time.LocalDate;

/* Груз
* customer: Покупатель
* origin: Место отправки
* destination: Место назначения
* originDate: Дата отправки
* destinationDate: Дата прибытия
* weight: Вес
* itinerary: План маршрута
* delivery: Доставка
*/

@Getter
@Setter
public class Cargo {
    private ObjectId id;
    private Customer customer;
    private Location origin, destination;
    private LocalDate originDate, destinationDate;
    private double weight;
    private Itinerary itinerary;
    private Delivery delivery;

    public Cargo(Customer customer, Location origin, Location destination,
                 LocalDate originDate, LocalDate destinationDate,
                 double weight, Itinerary itinerary, Delivery delivery) {
        this.customer = customer;
        this.origin = origin;
        this.destination = destination;
        this.originDate = originDate;
        this.destinationDate = destinationDate;
        this.weight = weight;
        this.itinerary = itinerary;
        this.delivery = delivery;
    }
}
