package com.ub.nextcargo.domen;

import lombok.Getter;
import lombok.Setter;

/*
* Местонахождение
*
* name: название пункта местонахождения
* address: адрес пункта местонахождения
 */

@Getter
@Setter
public class Location {
    private String name, address;

    public Location(String name, String address) {
        this.name = name;
        this.address = address;
    }
}
