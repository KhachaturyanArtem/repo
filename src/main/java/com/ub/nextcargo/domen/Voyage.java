package com.ub.nextcargo.domen;

import org.bson.types.ObjectId;

/*
* Рейс
*
* id : ObjectId
* number: Номер
* schedule: Расписание рейса
*/

public class Voyage {
    private ObjectId id;
    private String number;
    private Schedule schedule;

    public Voyage(String number, Schedule schedule) {
        this.number = number;
        this.schedule = schedule;
    }
}
