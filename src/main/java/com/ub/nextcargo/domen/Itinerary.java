package com.ub.nextcargo.domen;

import lombok.Getter;

import java.util.List;

/*
* План маршрута
*
* rotesStages: Этапы маршрута
*/

@Getter
public final class Itinerary {
    private List<RouteStage> rotesStages;

    public Itinerary(List<RouteStage> routeStage) {
        this.rotesStages = routeStage;
    }
}
