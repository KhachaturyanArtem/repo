package com.ub.nextcargo.domen;

// Статус маршрутизации

public enum RoutingStatus {
    NOT_ROUTED,
    ROUTED,
    MISROUTED
}
